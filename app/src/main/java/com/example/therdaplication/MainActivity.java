package com.example.therdaplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import static com.example.therdaplication.R.id.login;

public class MainActivity extends AppCompatActivity {

    EditText log;
    EditText email;
    EditText telephone;
    EditText pass;
    EditText repeatPass;
    Button ok;
    TextView complite;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        log = (EditText) findViewById(R.id.login);
        email = (EditText) findViewById(R.id.email);
        telephone = (EditText) findViewById(R.id.tel);
        pass = (EditText) findViewById(R.id.password);
        repeatPass = (EditText) findViewById(R.id.repeat_pass);
        ok = (Button) findViewById(R.id.btn_ok);
        complite = (TextView) findViewById(R.id.validation_info);


        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!log.getText().toString().isEmpty() && email.getText().toString().contains("@.") && (telephone.getText().toString().startsWith("+") && telephone.getText().toString().length() == 13 && pass.getText().toString().equals(repeatPass.getText().toString()))) {
                    complite.setText("Validation complete");

                }
                if (!email.getText().toString().contains("@.")) {
                    complite.setText("Email wrong");
                }
                if (!telephone.getText().toString().startsWith("+") && telephone.getText().toString().length() != 13) {
                    complite.setText("Phone wrong");
                }
                if (!pass.getText().toString().equals(repeatPass.getText().toString())) {
                    complite.setText("Passwords doesn`t equals");
                }
                if (log.getText().toString().isEmpty()) {
                    complite.setText("Wright your login");
                }
            }
        });
    }
}

